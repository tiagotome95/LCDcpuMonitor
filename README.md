# LCDcpuMonitor

LCDcpuMonitor is a CPU, RAM and disk usage monitor for 2x16 digit LCDs attached to an Arduino/ESP32 Board that communicate with your GNU/Linux system over Serial/USB.
You can use:

* simple (& in the future i2c) 2x16 digit LCDs,

The support for other Displays is planned, like:

* Nokia 5110 Monochronic Displays,
* i2c OLED & other

The Code is not complete for all Displays now, but it's and looks Powerful on your PC, NAS or Server and let you all times know how is the usage of your system, without the need to open an window or terminal.

## Dependencies

If you use a debian based, systemd managed distribution, you can install the dependencies with the installer `install.sh` (with root privileges). When not you can install the dependencies manually.

LCDcpuMonitor requires the installation of any dependencies:

* python3
* python3-systemd
* python3-pip (optional)
* pyserial
* python3-psutil
* git (installer)

this are the required python modules:

* python3-systemd
* psutil
* serial
* time
* os

## Hardware Architektures

* armhf (not recommended)
* arm64
* amd64

## Installation

you can install LCDcpuMonitor and all the dependencies on your debian based, systemd managed distribution with the installer `install.sh` (with root privileges). When you use a Distribution that can't handle `apt` commands, you can install the dependencies manually with the package manager of your distribution or/and with the help of `pip3`.

### Using the Installer

To use the installer just:

* download the installer, open a terminal and change to the directory where the installer `install.sh` is located, or

* open a terminal, clone the [repository](https://gitlab.com/tiagotome95/LCDcpuMonitor) of LCDcpuMonitor and change working directory:

```
git clone https://gitlab.com/tiagotome95/LCDcpuMonitor && cd LCDcpuMonitor
```

and run the installer:

```
sudo bash install.sh
```

or give the installer execute permissions:

```
sudo chmod +x install.sh
```
and then run the installer with:

```
sudo ./install.sh
```

### Dependencies Installation

You can install the dependancies with `apt` or with `apt & pip` if you prefer.

#### Install Dependencies with `apt` only:

Update `apt` list

```
sudo apt update
```

Install Dependencies:

```
sudo apt install python3 python3-systemd python3-serial python3-psutil
```

#### Install Dependencies with `apt` and `pip`:

Update `apt` list

```
sudo apt update
```

Install Dependencies:

```
sudo apt install python3 python3-systemd python3-pip
```

and the python modules with `pip3`:

```
pip3 install pyserial psutil
```

### LCDcpuMonitor service Installation

To install LCDcpuMonitor manually you have to copy the python script and the systemd service to the right paths and than reload daemons/services, if you have a prepared arduino board with a supported Display and the `LCDcpuMonitor.ino` flashed, you can then start and enable the service too.

####

copy python script to installation path:

```
cp -v LCDcpuMonitor.py /usr/local/bin
```

and give it execute permissions:

```
sudo chmod +x /usr/local/bin/LCDcpuMonitor.py
```

copy systemd service unit file to installation path to start LCDcpuMonitor at boot:

```
cp -v LCDcpuMonitor.service /etc/systemd/system
```

and give the right permissions:

```
sudo chmod 640 /etc/systemd/system/LCDcpuMonitor.service
```

#### Reload, Enable and Start service

Reload systemd daemons:

```
sudo systemctl daemon-reload
```
                
Enable `LCDcpuMonitor.service`:

```
sudo systemctl enable LCDcpuMonitor.service
```

Start `LCDcpuMonitor.service`:

```
sudo systemctl start LCDcpuMonitor.service
```


## Contributing

State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment

Tiago Simões Tomé [tiagotome95@sapo.pt](mailto:tiagotome95@sapo.pt) Thanks to all contributors working on NextCloud, ownCloud, and all other free (and open) software projects like Debian that lets you start in your favorite architecture :D

## License

The documentation in this project is licensed under the [Creative Commons Attribution-ShareAlike 4.0 license](https://choosealicense.com/licenses/cc-by-sa-4.0/), the source code content (also the source code included in the documentation) is licensed under the [GNU GPLv3 license](https://choosealicense.com/licenses/gpl-3.0/).
