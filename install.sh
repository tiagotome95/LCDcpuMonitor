#!/bin/bash
# ######################
# ###  LCDcpuMonitor  #
# #####################################################
# ### Author: Tiago Simões Tomé # ©2018-2019,2023  ###
# ###################################################
# ©2018-2019,2023
# 
# LCDcpuMonitor is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# See <http://www.gnu.org/licenses/>.

VERSION=$(git describe --exact-match --tags)
DEPENDENCIES="git python3 python3-systemd "
DEPENDENCIES_APT="python3-psutil python3-serial"
DEPENDENCIES_MODULES="psutil pyserial"
DEPENDENCIES_PIP="python3-pip"
REPO="https://gitlab.com/tiagotome95/LCDcpuMonitor"
TEMP_DIR="updated-installation-repo/"
BINARY="LCDcpuMonitor.py"
BINARY_DESTANY="/usr/local/bin/"
SERVICE="LCDcpuMonitor.service"
SERVICE_DESTANY="/etc/systemd/system/"

echo "LCDcpuMonitor $VERSION installer"
echo "This is LCDcpuMonitor installer. LCDcpuMonitor is a CPU, RAM and disk usage monitor for 2x16 digit LCDs attached to an Arduino/ESP32 Board that communicate with your GNU/Linux system over Serial/USB."
echo "You can use simple/i2c 2x16 digit LCDs, Nokia 5110 Monochronic Displays and i2c OLED too. The Code is not complete now for all Display but it is and looks Powerful on your PC, NAS or Server."
echo ""
echo "LCDcpuMonitor requires the installation of any dependencies (python3 & modules): $DEPENDENCIES$DEPENDENCIES_MODULES, if you prefer using pip3 the installer installs $DEPENDENCIES_PIP too."
echo "If you use a debian based systemd managed distribution, you can install the dependencies with this installer with apt or apt & pip. When not you can install them manually too."
echo ""
while true; do
    read -p "Do you want to update APT repository list and install dependencies with apt or apt & pip3 before install LCDcpuMonitor? (Y/a/p/n) " yn
    case $yn in
        [YyAa]* ) echo "Dependencies installation [apt]"
                echo "Check APT Repository for updates..."
                sudo apt update
                echo "Install dependencies: $DEPENDENCIES$DEPENDENCIES_APT"
                sudo apt install -y $DEPENDENCIES$DEPENDENCIES_APT
                echo "Dependencies installed with apt!"; break;;
        [Pp]* ) echo "Dependencies installation [apt & pip3]..."
                echo "Check APT Repository for updates..."
                sudo apt update
                echo "Install dependencies (with apt): $DEPENDENCIES$DEPENDENCIES_PIP"
                sudo apt install -y $DEPENDENCIES$DEPENDENCIES_PIP
                echo "Install required python3 modules with pip3: $DEPENDENCIES_MODULES"
                pip3 install $DEPENDENCIES_MODULES
                echo "Dependencies installed with pip3!"; break;;
        [Nn]* ) echo "Continue without dependencies."
                echo "For more information about the dependencies have a look @ https://gitlab.com/tiagotome95/LCDcpuMonitor"; break;;
        * ) echo "Please answer yes or no.";;
    esac
done
echo ""

while true; do
    read -p "Do you want to install LCDcpuMonitor? (Y/n) " yn
    case $yn in
        [Yy]* ) echo "Install LCDcpuMonitor..."
                echo ""
                # Create Temporary directory
                echo "Create temporary directory $(basename $TEMP_DIR) and clone (download) LCDcpuMonitor's Repository: $REPO"
                git clone $REPO $TEMP_DIR
                echo "LCDcpuMonitor sucefully downloaded to $(basename $TEMP_DIR)"
                echo ""
                cd $TEMP_DIR && VERSION=$(git describe --exact-match --tags) && cd ..;
                echo "LCDcpuMonitor $VERSION Installer"
                # Copy and chmod script and service unit file to installation path
                echo "Copy $BINARY script to $BINARY_DESTANY"
                cp -v $TEMP_DIR$BINARY $BINARY_DESTANY
                sudo chmod +x $BINARY_DESTANY$BINARY
                echo "Copy service unit file $SERVICE to $SERVICE_DESTANY"
                cp -v $TEMP_DIR$SERVICE $SERVICE_DESTANY
                sudo chmod 640 $SERVICE_DESTANY$SERVICE
                # Start and Enable Service
                echo "Reload systemd daemons."
                sudo systemctl daemon-reload
                echo "Enable $SERVICE."
                sudo systemctl enable $SERVICE
                echo "Start $SERVICE."
                sudo systemctl start $SERVICE
                # Remove Temporary directory
                echo "Remove temporary directory $(basename $TEMP_DIR)"
                rm -fr -v $TEMP_DIR
                echo ""
                echo "LCDcpuMonitor installed sucefully!"; break;;
        [Nn]* ) echo "Exit without installation!"
                echo ""; break;;
        * ) echo "Please answer yes or no.";;
    esac
done

echo ""

echo "What you don't like LCDcpuMonitor anymore? Really? OK, no problem! I was to lazy to make a uninstall.sh script..."
echo "...but we/I (me & meaw) decide that we should have something like an uninstallation possiblity, so when you think this software is Bullshit feel free to remove it and all tha shit aroud it! This includes all tha unneeded Dependencies!"
echo ""
echo "MEAWW!!!, we had the care to thing about your system and leave python3 on your Operation System, so if you need it for another program... CIAO"

while true; do
    read -p "Do you want uninstall LCDcpuMonitor? (Y/n) " yn
    case $yn in
        [Yy]* ) echo "unInstall LCDcpuMonitor..."
                echo ""
                echo "Install 1000s of TeraBytes of Porns ands Viruses on your Computer,.."
                echo ""
                echo "LCDcpuMonitor $VERSION unInstaller :'("
                #Stop and Disable service
                echo "Stop $SERVICE."
                sudo systemctl stop $SERVICE
                echo "Disable $SERVICE."
                sudo systemctl disable $SERVICE
                # remove program and service file from installation paths
                echo "Remove python script and service unit file from installation paths"
                echo "Remove $BINARY script from $BINARY_DESTANY"
                rm -v $BINARY_DESTANY$BINARY
                echo "Remove service unit file $SERVICE from $SERVICE_DESTANY"
                rm -v $SERVICE_DESTANY$SERVICE
                # Reload systemd daemons
                echo "Reload systemd daemons"
                sudo systemctl daemon-reload
                while true; do
                    read -p "Do you want to remove all the dependencies? Type (P) to remove the pip3 dependencies? (Y/p/n) " yn
                    case $yn in
                        [Yy]* ) echo "unInstall LCDcpuMonitor dependencies..."
                                sudo apt remove -y python3-systemd python3-psutil pyserial
                                echo "Dependencies Removed! [APT]"; break;;
                        [Pp]* ) echo "unInstall LCDcpuMonitor dependencies..."
                                sudo apt remove -y python3-systemd python3-pip
                                pip3 uninstall psutil pyserial
                                echo "Dependencies Removed! [APT&PIP]"; break;;
                        [Nn]* ) echo "Exit without uninstallation!"
                                echo ""; break;;
                        * ) echo "Please answer yes or no.";;
                    esac
                done
                while true; do
                    read -p "But, do you want to remove git too? (Y/n) " yn
                    case $yn in
                        [Yy]* ) echo "Remove git..."
                                sudo apt remove -y git
                                echo "All dependencies Removed!"; break;;
                        [Nn]* ) echo "Exit without uninstallation!"
                                echo ""; break;;
                        * ) echo "Please answer yes or no.";;
                    esac
                done
                echo "LCDcpuMonitor uninstalled sucefully!"; break;;
        [Nn]* ) echo "Exit without installation!"
                echo ""; break;;
        * ) echo "Please answer yes or no.";;
    esac
done

