/*#############################
* ###  LCDcpuMonitor v0.1.0  #
* #####################################################
* ### Author: Tiago Simões Tomé # ©2018-2019,2023  ###
* ###################################################
* ©2018-2019,2023
* 
* LCDcpuMonitor is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* any later version.
* 
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* See <http://www.gnu.org/licenses/>.
* 
* 
*/

#include <Wire.h> 
#include <LiquidCrystal.h>

const int rs = 12, en = 11, d4 = 5, d5 = 4, d6 = 3, d7 = 2;
LiquidCrystal lcd(rs, en, d4, d5, d6, d7);
String inData;
//const int buttonPin = 6;
//const int buttonPin2 = 7;
const int ledPin = 9;
const int contrastPin = 10;
unsigned int brightness = 170;
unsigned int contrast = 77;
//int buttonState = 0; 
//int buttonState2 = 0;

void setup() {
    //pinMode(buttonPin2, INPUT);
    //pinMode(buttonPin, INPUT);
    pinMode(ledPin, OUTPUT);
    analogWrite(ledPin, brightness);
    analogWrite(contrastPin, contrast);
    Serial.begin(9600);
    lcd.begin(16, 2);
    // Welcome Message
    lcd.setCursor(0, 0);
    lcd.print("  Power Mac G4");
    lcd.setCursor(0, 1);
    lcd.print("Debian GNU/Linux");
    delay(50);
}

void loop() {
  /*ledPin == HIGH;
  buttonState = digitalRead(buttonPin);
  buttonState2 = digitalRead(buttonPin2);
  if (buttonState == HIGH) {
      Serial.println("reboot");
      delay(250);
    
  }
  if (buttonState2 == HIGH) {
      Serial.println("shutdown");
      delay(250);
    
  }*/

    while (Serial.available() > 0)
    {
        char recieved = Serial.read();
        inData += recieved; 
        
        if (recieved == '*')
        {
            inData.remove(inData.length() -1, 1);
            lcd.setCursor(0,0);
            lcd.print(inData);
            inData = ""; 
            
            if(inData == "DIS")
            {
              delay(0); 
            }
  Serial.println("nothing");
            
        } 
        
        if (recieved == '#')
        {
            inData.remove(inData.length() -1, 1);
            lcd.setCursor(0,1);
            lcd.print(inData);
            inData = ""; 
        }
    }
}
