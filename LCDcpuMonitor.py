#!/usr/bin/python3
# ######################
# ###  LCDcpuMonitor  #
# #####################################################
# ### Author: Tiago Simões Tomé # ©2018-2019,2023  ###
# ###################################################
# ©2018-2019,2023
# 
# LCDcpuMonitor is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# See <http://www.gnu.org/licenses/>.

from systemd import journal
import psutil
import serial
import time
import os
ser = serial.Serial('/dev/ttyUSB0') #define serial port.
line = "nothing"; #create the variable where incoming communication is gonna be saved.
while True:
    sep = ','
    sep1 = '.'
    t = os.popen('uptime -p').read()[:-1] #open the file /proc/uptime and read it
    cpu = psutil.cpu_percent(interval=1) #reads the cpu avarage every second
    cpu = str(cpu).replace('"', "")# Remove unecessary characters from string
    cpu = str(cpu).replace("'", "")# --||--
    cpu = str(cpu).replace(' ', "")# --||--
    cpu = 'CPU', str(cpu).split(sep1, 1)[0] #split the string at the first dot and remove anything behind it
    amount_of_ram = str(psutil.virtual_memory()[2]).split(sep1, 1)[0] # --||--
    ram = 'RAM', str(amount_of_ram) # Convert the variable to a string. (Why??)
    uptime = t
    updisk = (str(uptime).split(sep, 1)[0],'du',psutil.disk_usage('/') [3], '% ') #generate a string that contains the uptime and how much of the / drive is used.
    downdisk = str(cpu) + '%  ' + str(ram) + '%  ' #generate a string that contains the info regarding the RAM and the CPU.
    downdisk = str(downdisk) #convert the variable to a string??
    upstr = str(updisk) #--||--
    upstr = upstr.replace("(", "") #replace all unecessary characters from the strings
    upstr = upstr.replace(",", "")
    upstr = upstr.replace("'", "")
    upstr = upstr.replace(")", "")
    upstr = upstr.replace("minutes", "m")
    upstr = upstr.replace("minute", "m")
    upstr = upstr.replace("hours", "h")
    upstr = upstr.replace("hour", "h")
    upstr = upstr.replace("days", "d")
    upstr = upstr.replace("day", "d")
    downdisk = downdisk.replace("(", "")
    downdisk = downdisk.replace(",", "")
    downdisk = downdisk.replace("'", "")
    downdisk = downdisk.replace(")", "")
    ser.write(b"*") # Let the arduino know we're about to write onto the second row
    ser.write(upstr.encode()) # Write the data
    ser.write(b"#") # Let the arduino know we're about to write onto the first row
    ser.write(downdisk.encode())

    line = ser.readline() # Read the serial output
    if (not line == b'nothing\r\n'): # if false any buttons were'nt pressed, do nothing.
        if (line == b'reboot\r\n'): # A reboot was ordered by pressing a button
            os.system("reboot") #reboot the host system, replace the command if you wish to do something else when a button is pressed.
        if (line == b'shutdown\r\n'): # A shutdown was ordered
            os.system("shutdown now")
    journal.send('LCDcpuMonitor.service: LCDcpuMonitor online.')
    time.sleep(2) # Wait two seconds
